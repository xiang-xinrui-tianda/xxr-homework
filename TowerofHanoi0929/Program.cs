﻿using System;

namespace Hanoi0929
{
    /// <summary>
    /// 汉诺塔程序
    /// 将柱A上的所有盘子转移到柱C，越底层的盘子越大，转移过程中始终是小盘子在上，大盘子在下
    /// </summary>
    internal class Program
    {
        static void Main(string[] args)
        {
            char Pillar1 = 'a';
            char Pillar2 = 'b';
            char Pillar3 = 'c';
            Console.Write("请输入一个大于0的整数：");
            int n = Convert.ToInt32(Console.ReadLine());
            Hanoi((int)n, Pillar1, Pillar2, Pillar3);
            Console.ReadKey();
        }
        //定义汉诺塔递归函数
        static void Hanoi(int n, char A, char B, char C)
        {
            if (n == 1) move(A, C);
            else
            {
                //把n-1个盘子从柱A经过柱C转移到柱B
                Hanoi(n - 1, A, C, B);
                //把柱A最底层的盘子转移到柱C
                move(A, C);
                //把这n-1个盘子从柱B转移到柱C
                Hanoi(n - 1, B, A, C);
            }
        }
        static void move(char M, char N)
        {
            Console.WriteLine($"把柱{M}最上面的盘子转移到柱{N}上");
        }
    }
}