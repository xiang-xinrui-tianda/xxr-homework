# 向欣芮的CSharp作业仓库



![输入图片说明](image.png)



姓名：向欣芮
学号：3021209234
教师：袁学民

#### 文件说明


- [光标移动游戏](https://gitee.com/xiang-xinrui-tianda/xxr-homework/blob/master/Program.cs)
- [反应快慢测试](https://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/Homework0927/ReactionTest)
- [打印今年的日历](http://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/CalendarHmoework0922)
- [输出比所给数小的最大质数，并统计计算时间](http://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/Homework0927/PrimeNumber)
- [汉诺塔程序](http://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/TowerofHanoi0929)
- [用最小二乘法预测GDP](https://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/LeastSquareMethod1005)
- [维吉尼亚加密](https://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/VirginiaEncryption1006)
- [维吉尼亚加密窗体版](https://gitee.com/xiang-xinrui-tianda/xxr-homework/tree/master/VirginiaEncrytion_WinForm)


