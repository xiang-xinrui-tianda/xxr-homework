﻿namespace HomeworkTwo0927
{
    internal class Program
    {
        /// <summary>
        /// 输出比所给数小的最大质数，并统计计算时间
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //确保有比输入的数还小的质数
            int intN = 1;
            while (intN <= 2)
            {
                Console.Write("请输入一个数：");
                string strN = Console.ReadLine();
                //把输入的strN转换成int型
                bool mybool = Int32.TryParse(strN, out intN);
            }
            int theMax = 0;
            //定义初始时间
            DateTime start = DateTime.Now;
            //寻找2到输入数之间最大的质数
            if (intN > 2)
            {
                for (int i = 2; i < intN; i++)
                {
                    int intM = 0;
                    for (int j = 2; j < i; j++)
                    {
                        if (i % j == 0)
                        {
                            intM = 1;
                            break;
                        }
                    }
                    if (intM == 0)
                    {
                        theMax = i;
                    }
                }
            }
            Console.WriteLine($"比{intN}小的最大质数是:{theMax}");
            //定义结束时间
            DateTime end = DateTime.Now;
            TimeSpan interval = end - start;
            Console.WriteLine("计算时间为："+ interval);
        }
    }
}