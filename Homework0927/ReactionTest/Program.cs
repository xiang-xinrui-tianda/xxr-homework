﻿namespace HomeworkOne0927
{
    /// <summary>
    /// 反应快慢测试
    /// </summary>
    internal class Program
    {  
        //设置测试次数
        const int Test = 10;

        public static ConsoleKey input { get; private set; }

        enum Direction
        {
            W,
            S,
            A,
            D
        }
        static void Main(string[] args)
        {
            //定义反应时间
            double ReactionTime = 0;
            //定义错误次数
            int error = 0;
            for (int i = 1; i <= Test; i++)
            {
                //清屏
                Console.Clear();
                //随机生成方位
                Random rnd = new Random();
                //显示测试次数，耗时和错误次数
                Console.WriteLine($"现在是第{i}次测试");
                ///利用随机数输出方位   
                Direction direction = (Direction)rnd.Next(0, 4);
                Console.WriteLine(direction.ToString());
                //定义初始时间
                DateTime start = DateTime.Now;
                bool flag = false;
                //测试输入正确与否
                do
                {
                    input = Console.ReadKey(true).Key;
                    switch (direction)
                    {
                        case Direction.W:
                            if (input == ConsoleKey.W)
                                flag = true;
                            else
                            {
                                flag = false;
                                error++;
                            }
                            break;
                        case Direction.S:
                            if (input == ConsoleKey.S)
                                flag = true;
                            else
                            {
                                flag = false;
                                error++;
                            }
                            break;
                        case Direction.A:
                            if (input == ConsoleKey.A)
                                flag = true;
                            else
                            {
                                flag = false;
                                error++;
                            }
                            break;
                        case Direction.D:
                            if (input == ConsoleKey.D)
                                flag = true;
                            else
                            {
                                flag = false;
                                error++;
                            }
                            break;
                        default:
                            break;
                    }
                } while (flag == false) ;
                 //定义本次测试结束时间
                 DateTime end = DateTime.Now;
                 ReactionTime += (end - start).TotalSeconds;
                Console.WriteLine($"目前耗时{ReactionTime},错误次数为{error}");
                Console.ReadKey(true);
            }
        }
    }
}