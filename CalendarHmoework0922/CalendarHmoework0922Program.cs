﻿namespace CalendarHmoework0922
{
   
    internal class Program
    {
        const int MonthWidth = 32;
        const int MonthHeight = 10;
        /// <summary>
        /// 打印今年的日历
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //初始化日期
            DateTime theDay = new DateTime(2022, 1, 1);
            
            //初始化光标位置
            int x = 0, y = 0;
            Console.SetCursorPosition(x, y);

            do
            {
                int month = theDay.Month;
                for (; month < 13; month++)
                {
                    //确定月份的行位置
                    y = 0;
                    Console.SetCursorPosition(x, y);
                    int intN = ((month-1) / 3) ;
                    y += intN*MonthHeight;
                    Console.SetCursorPosition(x, y);
                    //打印每个月表头
                    int intM = (month - 1)%3;
                    x = intM * MonthWidth;
                    Console.SetCursorPosition(x, y);
                    Console.Write(month + "月");
                    y++;
                    Console.SetCursorPosition(x, y);
                    Console.Write("日  一  二  三  四  五  六");
                    //确定第一天是星期几
                    int intT = (int)theDay.DayOfWeek;
                    x += 4 * intT;
                    y++;
                    Console.SetCursorPosition(x, y);
                    //打印每个月的日历
                    do
                    {
                        if ((int)theDay.DayOfWeek == 6)
                        {
                            Console.Write(theDay.Day);
                            x = intM * MonthWidth;
                            y++;
                            Console.SetCursorPosition(x, y);
                        }
                        else
                        {
                            Console.Write(theDay.Day);
                            x += 4;
                            Console.SetCursorPosition(x, y);
                        }
                        theDay = theDay.AddDays(1);  
                    } while (month == theDay.Month);
                }
            }while (theDay.Year == 2022);
            Console.ReadKey(true);

        }
    }
}