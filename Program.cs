﻿namespace Homework0920
{
    internal class Program
    {
        /// <summary>
        /// 光标移动游戏
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //定义并初始化坐标（x，y）
            int x = 0, y = 0;
            //定义输入
            ConsoleKey input;
            do
            {   //清除控制台缓冲区和相应的控制台窗口的显示信息
                Console.Clear();
                //定义光标位置
                Console.SetCursorPosition(x, y);
                Console.Write("Hello World!");
                //用户输入
                input = Console.ReadKey(true).Key;
                //根据用户输入调整坐标
                switch (input)
                {
                    case ConsoleKey.W:
                        if (y > 0)
                            y--;
                        break;
                    case ConsoleKey.S:
                        if (y < 50)
                            y++;
                        break;
                    case ConsoleKey.A:
                        if (x > 0)
                            x--;
                        break;
                    case ConsoleKey.D:
                        if (x <50)
                            x++;
                        break;
                    default: 
                        break;
                }

            }
            //回到第二步
            while (input != ConsoleKey.Enter);
        }
    }
}