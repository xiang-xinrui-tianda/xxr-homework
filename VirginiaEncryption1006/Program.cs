﻿using Test1006.Models;

namespace Test1006
{
    internal class Program
    {
        /// <summary>
        /// 维吉尼亚加密测试
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //用户输入明文和密钥
            Console.Write("请输入明文:");
            String Plain = Console.ReadLine();
            Console.Write("请输入密钥:");
            String Key = Console.ReadLine();
            //加密
            MyEncoder myEncoder = new MyEncoder();
            myEncoder.Plaintext = Plain;
            myEncoder.Key = Key;
            myEncoder.Encode();
            //输出密文
            Console.WriteLine($"密文是:{myEncoder.Cipher}");
        }
    }
}