﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1006.Models
{
    /// <summary>
    /// 加密类
    /// </summary>
    internal class MyEncoder
    {
        /// <summary>
        /// 明文
        /// </summary>
        public string Plaintext { get; set; }

        /// <summary>
        /// 密文
        /// </summary>
        public String Cipher { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        public String Key { get; set; }

        /// <summary>
        /// 加密动作
        /// </summary>
        public void Encode()
        {
            //ToDo 加密过程
            int Key_Length = Key.Count();
            Key.ToArray();
            int Plaintext_Length = Plaintext.Count();
            Plaintext.ToArray();

            //计算出密文字符对应的数字
            int[] CipherString = new int[Plaintext_Length];
            for (int i = 0, j = 0; j < Plaintext_Length; j++)
            {
                if (i != Key_Length - 1)
                {
                    CipherString[j] = Plaintext[j] + Key[i] - 96;
                    i++;
                }
                else
                {
                    CipherString[j] = Plaintext[j] + Key[i] - 96;
                    i = 0;
                }
            }
            //将数字转换为密文
            for (int i = 0; i < Plaintext_Length; i++)
            {
                while(CipherString[i] >122)
                {
                    CipherString[i] = CipherString[i] - 26;
                }
                Cipher += Convert.ToChar(CipherString[i]);
            }
        }
    }
}
